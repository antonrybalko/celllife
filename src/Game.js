/* global cancelAnimationFrame, requestAnimationFrame */
import { stats } from './globals'
import LinkedList from './LinkedList'

export default class Game {
  constructor (botGenerator = null, environment = null, display = null, params = []) {
    // Game params
    this.viewMode = params['viewMode'] || 'default'
    this.fps = params['fps'] || 100
    this.stepInterval = params['stepInterval'] || 10
    this.autoReinit = params['autoReinit'] != null ? params['autoReinit'] : true

    // Display and bot generator objects
    this.botGenerator = botGenerator
    this.display = display
    this.env = environment

    // Game states
    this.paused = true
    this.stepHandle = null
    this.animationHandle = null

    // Internal fields
    this.selected = null

    this.init()
  }

  init () {
    this.stepCount = 0
    this.botsAlive = 0
    // TODO: fill all stats with zero...
    stats.diedAvg.health = 0

    // Init cells and generate bots
    this.env.init()
    this.seed()
    this.update()
  }

  seed () {
    if (this.botGenerator === null) {
      return
    }

    this.bots = new LinkedList()
    for (let bot of this.botGenerator.generate(this)) {
      if (this.getCollisions(bot, bot.x, bot.y).length) {
        bot.die('random', true)
      }

      const cell = this.env.getCellForCoords(bot.x, bot.y)
      bot.cell = cell
      bot.cellNode = cell.addContent(bot)
    }
  }

  addBot (bot) {
    return this.bots.push(bot)
  }

  removeBot (node) {
    this.bots.remove(node)
  }

  setSelected (selected) {
    if (this.selected) {
      this.selected.isSelected = false
      if (this.selected.cell) {
        this.selected.cell.isSelected = false
      }
    }
    this.selected = selected
    if (this.selected) {
      this.selected.isSelected = true
      if (this.selected.cell) {
        this.selected.cell.isSelected = true
      }
    }
    console.log(selected)
  }

  getSelected () {
    return this.selected
  }

  stepCells () {
    this.env.step(this)

    for (let node = this.bots.tail; node !== null; node = node.previous) {
      // TODO: gravity must be inside environment step. Optimization?
      if (Math.random() <= 0.9 && !node.data.alive) {
        node.data.moveBot(this.env.gravityVector)
      }
      node.data.step()
    }
  }

  step () {
    const startTime = new Date()
    stats.died.total = 0
    stats.died.age = 0
    stats.died.health = 0
    stats.died.resources = 0
    stats.died.attack = 0
    stats.died.attacking = 0
    stats.died.grow = 0

    this.stepCells()

    this.stepCount += 1
    stats.diedAvg.total += stats.died.total
    if (stats.died.total > 0) {
      var cumulativePart = 0.8
      stats.diedAvg.age = cumulativePart * stats.diedAvg.age + (1 - cumulativePart) * stats.died.age / stats.died.total
      stats.diedAvg.health = cumulativePart * stats.diedAvg.health + (1 - cumulativePart) * stats.died.health / stats.died.total
      stats.diedAvg.resources = cumulativePart * stats.diedAvg.resources + (1 - cumulativePart) * stats.died.resources / stats.died.total
      stats.diedAvg.attack = cumulativePart * stats.diedAvg.attack + (1 - cumulativePart) * stats.died.attack / stats.died.total
      stats.diedAvg.attacking = cumulativePart * stats.diedAvg.attacking + (1 - cumulativePart) * stats.died.attacking / stats.died.total
      stats.diedAvg.grow = cumulativePart * stats.diedAvg.grow + (1 - cumulativePart) * stats.died.grow / stats.died.total
    }
    stats.stepTime = (new Date() - startTime)

    if (this.autoReinit && this.botsAlive === 0) {
      this.init()
    }
  }

  update () {
    if (this.display) {
      this.display.update(this.env, this.bots, this.selected, this.viewMode)
      this.display.render()
    }
  }

  start () {
    this.paused = false
    this.gameRun()
    this.animationRun()
  }

  stop () {
    if (this.stepHandle) {
      clearTimeout(this.stepHandle)
    }
    if (this.animationHandle) {
      cancelAnimationFrame(this.animationHandle)
    }
    this.paused = true
    this.update()
  }

  togglePause () {
    if (this.paused) {
      this.start()
    } else {
      this.stop()
    }
  }

  animationRun () {
    if (this.animationHandle) {
      cancelAnimationFrame(this.animationHandle)
    }

    let timeLast = new Date()
    stats.animationRunTime = timeLast
    stats.animationRunstepCount = 0

    // TODO: arrow function
    const self = this
    this.animationHandle = requestAnimationFrame(function animate () {
      if (!self.paused) {
        const timeNow = new Date()
        const timePassed = timeNow - timeLast
        //    this.display.update(this.cells, window.debug.viewMode);
        // }
        if (timePassed >= 1000 / self.fps) {
          self.update()
          timeLast = timeNow
          stats.animationRunstepCount++
          stats.framePerSecond = stats.animationRunstepCount / (timeNow - stats.animationRunTime) * 1000
        }
        self.animationHandle = requestAnimationFrame(animate)
      }
    })
  }

  gameRun () {
    stats.gameRunTime = new Date()
    stats.gameRunStepCount = 0
    // TODO: arrow function
    const self = this
    this.stepHandle = setTimeout(function run () {
      if (!self.paused) {
        self.step()
        stats.gameRunStepCount++
        stats.stepPerSecond = stats.gameRunStepCount / ((new Date()) - stats.gameRunTime) * 1000
        self.stepHandle = setTimeout(run, self.stepInterval)
      }
    }, self.stepInterval)
  }

  getCollisions (bot, x, y, moveVector = null) {
    // Get neighbour 9 cells
    let neighbourCells = []
    for (let cellOffsetX = -1; cellOffsetX <= 1; cellOffsetX++) {
      for (let cellOffsetY = -1; cellOffsetY <= 1; cellOffsetY++) {
        const cellCoord = this.env.boundXY(x + cellOffsetX, y + cellOffsetY)
        const cell = this.env.getCellForCoords(cellCoord.x, cellCoord.y)
        if (!neighbourCells.includes(cell)) {
          neighbourCells.push(cell)
        }
      }
    }

    // Get all neighbours from cells
    let neighbours = []
    for (const cell of neighbourCells) {
      neighbours.push(...cell.content)
    }

    // Check collistions
    let collistions = []
    const radius = bot.getRadius()
    for (const neighbour of neighbours) {
      if (!neighbour || neighbour === this) {
        continue
      }

      if (moveVector && ((moveVector.x > 0 && (neighbour.x - x)) < 0 ||
        (moveVector.x < 0 && (neighbour.x - x)) > 0 ||
        (moveVector.y > 0 && (neighbour.y - y)) < 0 ||
        (moveVector.y < 0 && (neighbour.y - y)) > 0)) {
        continue
      }
      const dx = x - neighbour.x
      const dy = y - neighbour.y
      const distance = Math.sqrt(dx * dx + dy * dy)

      if (distance < radius + neighbour.getRadius()) {
        collistions.push(neighbour)
      }
    }

    return collistions
  }
}
