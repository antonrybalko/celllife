import { samples } from './samples'

export default class BotGenerator {
  constructor (botClass, identityClass, number, numCellsX, numCellsY, health, factors, resources) {
    this.botClass = botClass
    this.identityClass = identityClass
    this.numCellsX = numCellsX
    this.numCellsY = numCellsY
    this.number = number
    this.health = health
    this.factors = factors
    this.resources = resources
  }

  generate (game) {
    const bots = []

    for (let sample of samples) {
      bots.push(this.createBot(game, this.createIdentity(sample)))
    }

    for (var i = 0; i < this.number; i++) {
      bots.push(this.createBot(game, this.createIdentity()))
    }

    return bots
  }

  createBot (game, identity) {
    return new this.botClass( // eslint-disable-line
      game,
      null,
      Math.floor(Math.random() * this.numCellsX),
      Math.floor(Math.random() * this.numCellsY),
      Math.random(),
      identity,
      this.health,
      this.factors,
      this.resources
    )
  }

  createIdentity (identity) {
    return new this.identityClass(identity) // eslint-disable-line
  }
}
