import { stats } from './globals'
import Color from './Color'

function updateGameInfo (game, interval) {
  setInterval(function () {
    const selected = game.getSelected()
    document.getElementById('game-statistics').innerHTML =
            `Time: ${game.stepCount} ${getSeason(game)}<br>` +
            `Bots alive: ${game.botsAlive}<br>` +
            `Max age: ${stats.maxAge} <br>` +
            /* 'Max generation: ' + stats.maxGeneration + '<br>' +
            'Max mutations: ' + stats.maxMutations + '<br>' + */
            `Died by health: ${Math.round(stats.diedAvg.health * 100)}%<br>` +
            `Died by age: ${Math.round(stats.diedAvg.age * 100)}%<br>` +
            `Died by attack: ${Math.round(stats.diedAvg.attack * 100)}%<br>` +
            `Died by attacking: ${Math.round(stats.diedAvg.attacking * 100)}%<br>` +
            `Died by resource: ${Math.round(stats.diedAvg.resources * 100)}%<br>` +
            `Died by grow: ${Math.round(stats.diedAvg.grow * 100)}%<br>` +
            // 'Step time: ' + stats.stepTime + '<br>' +
            `Step/sec: ${stats.stepPerSecond.toFixed(1)}<br>` +
            `Frame/sec: ${stats.framePerSecond.toFixed(1)}`

    if (selected) {
      document.getElementById('cell-info').innerHTML = ''
      renderCellInfo(selected)
      if (selected.type === 'bot' && selected.cell) {
        renderCellInfo(selected.cell)
      }
    }
  }, interval)
}

function getSeason (game) {
  const seasonName = game.env.getSeasonName(game.stepCount)
  const seasons = {
    'summer': '<i class="fas fa-sun game-season-summer"></i>',
    'autumn': '<i class="fas fa-leaf game-season-autumn"></i>',
    'winter': '<i class="fas fa-snowflake game-season-winter"></i>',
    'spring': '<i class="fas fa-seedling game-season-spring"></i>'
  }
  return seasonName in seasons ? seasons[seasonName] : seasonName
}

function showCellInfo (game, e) {
  const worldPos = game.display.toWorldPosition(e.offsetX, e.offsetY)
  const gameY = worldPos.y / game.env.cellHeight
  const gameX = worldPos.x / game.env.cellWidth
  const y = Math.floor(gameY)
  const x = Math.floor(gameX)
  if (y < 0 || y >= game.env.numCellsY || x < 0 || x >= game.env.numCellsX) {
    return
  }

  const cell = game.env.cells[y][x]
  if (!cell.isEmpty()) {
    let selected = null
    for (const item of cell.content) {
      if (!item) {
        continue
      }
      const dx = gameX - item.x
      const dy = gameY - item.y
      const distance = Math.sqrt(dx * dx + dy * dy)

      if (distance < item.size * 0.6) {
        selected = item
        break
      }
    }
    if (selected) {
      game.setSelected(selected)
    } else {
      game.setSelected(cell)
    }
  } else {
    game.setSelected(cell)
  }
  game.update()
}

function hideCellInfo (game) {
  game.setSelected(null)
  game.update()
}

function renderCellInfo (selected) {
  let color = 'white'
  let text = ''
  if (selected.type === 'cell') {
    color = new Color(selected.getFillColor()).toHexStr()
    text = 'Cell'
  } else if (selected.type === 'bot') {
    color = new Color(selected.getColor()).toHexStr()
    text = selected.name
  }

  const infoEl = document.getElementById('cell-info')
  infoEl.appendChild(renderTitleElement(color, text))
  if (selected.type === 'bot') {
    infoEl.appendChild(renderHealth(selected))
    infoEl.appendChild(renderResources(selected))
    infoEl.appendChild(renderStats(selected))
    infoEl.appendChild(renderIdentityBarcode(selected))
  }
  infoEl.appendChild(renderPropsElement(selected))
}

function renderTitleElement (objectColor, text) {
  const titleEl = document.createElement('div')
  titleEl.classList.add('cell-title')
  titleEl.innerHTML =
        '<div class="cell-color" style="background-color: ' + objectColor + ';"></div>' +
        '<div class="cell-text">' + text + '</div>'
  return titleEl
}

function renderHealth (object) {
  const health = Math.round(object.health * 1000) / 10
  const icon = object.alive ? '<i class="fas fa-heart cell-health-alive"></i>' : '<i class="fas fa-skull-crossbones cell-health-dead"></i>'
  return renderResourceBar(health, icon, 'health')
}

function renderIdentityBarcode (object) {
  const element = document.createElement('div')
  element.classList.add('cell-info-identity')
  const barWidth = 3
  object.identity.genome.forEach((gene, index) => {
    const color = new Color(object.identity.floatColorToInt(gene)).toHexStr()
    const size = Math.abs(gene) * 100
    const geneElement = document.createElement('div')
    geneElement.classList.add('cell-info-identity-gene')
    geneElement.style = `width: 100%; height: ${barWidth}px`
    geneElement.title = `${index}: ${gene}`
    const barElement = document.createElement('div')
    barElement.style = `background-color: ${color}; width: ${size}%; height: 100%`
    geneElement.appendChild(barElement)
    element.appendChild(geneElement)
  })
  return element
}

function renderResources (object) {
  const factorIcons = {
    'light': '<i class="fas fa-sun"></i>',
    'temperature': '<i class="fas fa-thermometer-half"></i>'
  }
  const resourceIcons = {
    'minerals': '<i class="fas fa-apple-alt"></i>'
  }
  const element = document.createElement('div')
  element.classList.add('cell-info-resources')
  for (const [factorName, value] of Object.entries(object.factors)) {
    const icon = factorName in factorIcons ? factorIcons[factorName] : factorName
    element.appendChild(renderResourceBar(Math.round(value * 1000) / 10, icon))
  }
  for (const resource of object.resourceNames) {
    const value = Math.round(object.resources[resource] * 1000) / 10
    const icon = resource in resourceIcons ? resourceIcons[resource] : resource
    element.appendChild(renderResourceBar(value, icon))
  }
  return element
}

function renderResourceBar (value, icon, resourceName) {
  const element = document.createElement('div')
  element.classList.add('cell-info-resource')
  element.innerHTML = `<div class="cell-info-resource-bar ${resourceName}" style="width: ${value}%"> ${icon} ${value} %</div>`
  return element
}

function renderStats (object) {
  const element = document.createElement('div')
  element.style = 'margin-bottom: 10px'
  element.innerHTML = `Age: ${object.stats.age} <br>Breedings: ${object.stats.breedings}<br>Generation: ${object.stats.generation}<br>Die reason: ${object.dieReason}`
  return element
}

function renderPropsElement (object) {
  const propsEl = document.createElement('ul')
  propsEl.innerHTML = renderPropsList(object)
  return propsEl
}

function renderPropsList (obj, stack = '') {
  const ignore = ['game', 'cell', 'cells', 'content', 'node', 'cellNode', 'identity']
  let result = ''
  for (let property in obj) {
    if (ignore.indexOf(property) < 0 && obj.hasOwnProperty(property)) {
      if (typeof obj[property] === 'object') {
        result += renderPropsList(obj[property], stack + property + '.')
      } else {
        if (typeof obj[property] === 'number') {
          result += `<li>${stack + property}: ${Math.round(obj[property] * 10000) / 10000} </li>`
        } else if (typeof obj[property] === 'string' || typeof obj[property] === 'boolean') {
          result += `<li>${stack + property}: ${obj[property]}</li>`
        }
      }
    }
  }
  if (obj && obj.type && obj.type === 'bot') {
    result +=
            '<textarea id="copy-bot-text" style="position: absolute; top: -9999px; left: -9999px;opacity: 0;">' +
            `// ${obj.identity.getName()} ${obj.identity.getSurname()}\n` +
            `${JSON.stringify(obj.identity.genome)},\n` +
            '</textarea>'
  }
  return result
}

function showCursorInfo (game, e) {
  const worldPos = game.display.toWorldPosition(
    e.pageX - e.target.offsetLeft,
    e.pageY - e.target.offsetTop
  )
  const x = Math.round(worldPos.x / game.env.cellWidth)
  const y = Math.round(worldPos.y / game.env.cellHeight)
  const coorldsEl = document.getElementById('mouse-coords')
  coorldsEl.classList.add('clearfix')
  coorldsEl.innerHTML = '<div style="float: left; width: 5em">X: ' + x + '</div><div style="float: left; width: 5em">Y: ' + y + '</div>'
}

export { updateGameInfo, showCellInfo, showCursorInfo, hideCellInfo }
