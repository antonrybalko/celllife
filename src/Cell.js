import { debug, CONST_CELL, COLORS } from './globals'
import Color from './Color'
import LinkedList from './LinkedList'

export default class Cell {
  constructor (environment, x, y, resources = null) {
    this.type = 'cell'
    this.color = COLORS.cell
    this.opacity = 0.99 * (1 - 1 / environment.numCellsY)
    this.lightDiffusion = 0.9 // 1 - all objects 100% transparent
    this.env = environment
    this.x = x
    this.y = y
    this.resources = resources
    this.content = new LinkedList()
    this.isSelected = false
    this.cells = environment ? environment.cells : []
    if (resources === null) {
      this.resources = this.getDefaultResources()
    }
  }

  isEmpty () {
    return this.content.isEmpty()
  }

  getProps () {
    const minerals = this.resources.minerals
    let light = this.resources.light
    let contentOpacity = 1
    for (const item of this.content) {
      if (item) {
        contentOpacity -= item.getProps().opacity * item.size
      }
    }
    if (contentOpacity !== 1) {
      light = light * (this.lightDiffusion + (1 - this.lightDiffusion) * contentOpacity)
    }
    return {
      light: light,
      minerals: minerals,
      temperature: this.resources.temperature,
      opacity: (1 - minerals) * this.opacity,
      hardness: minerals
    }
  }

  draw (display, viewMode) {
    const color = this.getFillColor(viewMode)
    if (color !== null || debug.grid) {
      const pos = this.getCellPos(display)
      if (!debug.grid) {
        display.graphics.lineStyle(0)
      } else {
        display.graphics.lineStyle(1, 0xFFFF00)
      }
      if (color) {
        display.graphics.beginFill(color, this.getFillAlpha(viewMode))
      } else {
        display.graphics.beginFill(0, 1)
      }
      display.graphics.drawRect(pos.x, pos.y, display.cellWidth, display.cellHeight)
      display.graphics.endFill()
    }
  }

  getCellPos (display) {
    return {
      x: this.x * display.cellWidth,
      y: this.y * display.cellHeight
    }
  }

  getFillColor (viewMode = 'env') {
    let color = null
    if (viewMode === 'env') {
      color = new Color(this.color)
        .mix(COLORS.minerals, this.resources.minerals)
      const light = this.getProps().light
      if (light > 0.5) {
        color.brighten((light - 0.5) * 2)
      } else {
        color.darken((0.5 - light) * 1.5)
      }
      // .desaturate(this.resources.minerals)
      color = color.color
    } else if ((viewMode === 'temperature' || viewMode === 'minerals' || viewMode === 'light') && this.resources[viewMode] !== 0) {
      color = COLORS.resource
    }
    return color
  }

  getFillAlpha (viewMode = 'env') {
    let alpha = 1
    if (viewMode !== 'env') {
      alpha = this.resources[viewMode]
    }
    return alpha
  }

  step (game) {
    this.recoverResources(game)
  }

  recoverResources (game) {
    this.recoverResource(game, 'light', this.x / this.env.numCellsX, this.y / this.env.numCellsY)
    this.recoverResource(game, 'temperature', this.x / this.env.numCellsX, this.y / this.env.numCellsY)
    this.recoverResource(game, 'minerals', this.x / this.env.numCellsX, this.y / this.env.numCellsY)
  }

  recoverResource (game, resource, xRatio, yRatio) {
    const optimum = this.resourceFunction(game, resource, xRatio, yRatio)
    if (optimum === null) {
      if (this.isSelected && resource === debug.resource) {
        console.log('resource function not defined for ', resource)
      }
      return 0
    }
    const delta = (optimum - this.resources[resource]) * (CONST_CELL.RECOVERY_SPEED[resource] || 0.5)
    if (this.isSelected && resource === debug.resource) {
      console.log('----------------')
      console.log('cell.recoverResources', this.resources[resource], optimum, delta)
    }
    const actualDelta = this.changeResource(resource, delta)
    if (this.isSelected && resource === debug.resource) {
      console.log('cell.recoverResources end', this.resources[resource])
    }
    return actualDelta
  }

  resourceFunction (game, resource, xRatio, yRatio) {
    switch (resource) {
      case 'minerals':
        return Math.max(
          0.1 + 0.5 * yRatio,
          this.volcano(xRatio, yRatio, 0.2, 1.1, 0.1) * 1,
          this.volcano(xRatio, yRatio, 4, 0.8, 0.2, 0.2, 1.5) * 1)
      case 'temperature':
        return Math.max(
          0.1 + 0.8 * (1 - yRatio) * (0.5 + 0.5 * this.seasonRatio()),
          this.volcano(xRatio, yRatio, 0.2, 1.1, 0.1) * 0.5,
          this.volcano(xRatio, yRatio, 4, 0.8, 0.2, 0.2, 1.5) * 0.5)
      case 'light':
        // old gradient light: const optimum = 0.1 + 0.8 * (1 - yRatio) * (0.5 + 0.5 * this.seasonRatio());
        if (this.y === 0 || !this.cells) {
          return 0.9 * (0.5 + 0.5 * this.seasonRatio())
        } else {
          // smooth oblique shadows
          const neighborsRange = { from: -2, to: +2 }
          const neighborsLight = []
          for (let xOffset = neighborsRange.from; xOffset <= neighborsRange.to; xOffset += 1) {
            const x = this.env.boundX(this.x + xOffset, this.y)
            neighborsLight.push({
              xOffset,
              light: this.cells[this.y - 1][x].getProps().light
            })
          }

          const topLight = this.cells[this.y - 1][this.x].getProps().light
          let light = 0
          let count = 0
          for (const nighborLight of neighborsLight) {
            // here is the magic
            if (nighborLight.xOffset >= 0 || nighborLight.light >= topLight) {
              light += nighborLight.light
              count += 1
            }
          }

          light = light / count
          return light * this.opacity // Absorption of light in water
        }
    }
    return null
  }

  getDefaultResources (game = null) {
    return {
      light: this.resourceFunction(game, 'light', this.x / this.env.numCellsX, this.y / this.env.numCellsY),
      minerals: this.resourceFunction(game, 'minerals', this.x / this.env.numCellsX, this.y / this.env.numCellsY),
      temperature: this.resourceFunction(game, 'temperature', this.x / this.env.numCellsX, this.y / this.env.numCellsY)
    }
  }

  seasonRatio (game) {
    let ratio = 1
    // Everithing is fine first year
    if (!game || this.env.seasonSteps === null || game.stepCount <= this.env.seasonSteps) {
      return ratio
    }

    let season = game.stepCount % this.env.seasonSteps
    let seasonNormal = season / this.env.seasonSteps

    if (season <= this.env.seasonSteps / 2) {
      ratio = 1 - seasonNormal * 2
    } else {
      ratio = seasonNormal * 2 - 1
    }

    return ratio
  }

  volcano (xRatio, yRatio, x0, y0, r, rx = 1, ry = 1) {
    const currentRadius = Math.pow(xRatio / rx - x0, 2) + Math.pow(yRatio / ry - y0, 2)
    return 1 - currentRadius / r
  }

  changeResource (resource, delta) {
    if (this.isSelected && resource === debug.resource) {
      console.log('cell.changeResource', this.resources[resource], delta)
    }
    let newValue = Math.min(1, Math.max(0, this.resources[resource] + delta))
    const actualDelta = newValue - this.resources[resource]
    this.resources[resource] = newValue
    if (this.isSelected && resource === debug.resource) {
      console.log('cell.changeResource end', this.resources[resource])
    }
    return actualDelta
  }

  addContent (item) {
    return this.content.push(item)
  }

  removeContent (node) {
    this.content.remove(node)
  }
};
