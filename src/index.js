// import 'fontawesome-free/css/all.scss'
import './css/style.css'
import { gameInit, botGeneratorInit, environmentInit } from './globals'
import Game from './Game'
import BotState from './BotState'
import BotGenerator from './BotGenerator'
import Environment from './Environment'
import Identity from './Identity'
import Display from './Display'
import { updateGameInfo, showCursorInfo, showCellInfo, hideCellInfo } from './gameInfo'

document.addEventListener('DOMContentLoaded', function (event) {
  // Create global game object
  var game = new Game(
    new BotGenerator(
      BotState,
      Identity,
      ...botGeneratorInit
    ),
    new Environment(
      ...environmentInit
    ),
    new Display(
      'game-canvas',
      ...environmentInit
    ),
    gameInit
  )
  window.game = game

  // Buttons to start / stop the game
  document.getElementById('play-btn').addEventListener('click', () => game.togglePause())
  document.getElementById('step-btn').addEventListener('click', () => {
    game.stop()
    game.step()
    game.update()
  })
  document.getElementById('new-btn').addEventListener('click', () => {
    game.init()
  })
  document.getElementById('reset-zoom-btn').addEventListener('click', () => {
    game.display.stickSelected = false
    game.display.resetScale()
    game.display.resetPosition()
  })

  // Canvas events
  const gameCanvasEl = document.getElementById('game-canvas')

  // Display mouse coords
  gameCanvasEl.addEventListener('mousemove', (e) => {
    showCursorInfo(game, e)
  })

  // Zoom
  gameCanvasEl.addEventListener('wheel', (e) => {
    game.display.changeScale(e.deltaY, e.offsetX, e.offsetY)
    e.preventDefault()
  })

  // Move canvas
  var mouseLastPos = null
  var mouseMoved = false
  gameCanvasEl.addEventListener('mousedown', (e) => {
    mouseLastPos = { x: e.offsetX, y: e.offsetY }
    mouseMoved = false
  })
  gameCanvasEl.addEventListener('mouseup', (e) => {
    if (!mouseMoved) {
      showCellInfo(game, e)
    }
    mouseLastPos = null
  })
  gameCanvasEl.addEventListener('mousemove', (e) => {
    mouseMoved = true
    if (mouseLastPos) {
      game.display.stickSelected = false
      game.display.changePosition(e.offsetX - mouseLastPos.x, e.offsetY - mouseLastPos.y)
      mouseLastPos = { x: e.offsetX, y: e.offsetY }
      return false
    }
  })

  // Change view mode
  for (let viewModeBtn of document.querySelectorAll('.view-mode')) {
    viewModeBtn.addEventListener('click', (e) => {
      game.viewMode = e.target.dataset.mode
      game.update()
    })
  }

  // Set fps / step interval
  document.getElementById('fps-input').value = game.fps
  document.getElementById('fps-btn').addEventListener('click', () => {
    game.fps = document.getElementById('fps-input').value
  })

  document.getElementById('step-interval-input').value = game.stepInterval
  document.getElementById('step-interval-btn').addEventListener('click', () => {
    game.stepInterval = document.getElementById('step-interval-input').value
  })

  for (let input of document.querySelectorAll('input:not([data-btn=""])')) {
    input.addEventListener('keyup', event => {
      if (event.key !== 'Enter') return

      document.getElementById(input.dataset.btn).click()
      event.preventDefault()
    })
  }

  // Select / deselect game objects
  document.getElementById('deselect-btn').addEventListener('click', (e) => {
    hideCellInfo(game, e)
    document.getElementById('cell-info').innerHTML = ''
  })

  document.getElementById('copy-bot-btn').addEventListener('click', () => {
    document.getElementById('copy-bot-text').select()
    document.execCommand('copy')
  })

  document.getElementById('stick-display-btn').addEventListener('click', () => {
    game.display.stickSelected = true
    game.update()
  })

  // Show game info with specified interval
  updateGameInfo(game, 100)
})
