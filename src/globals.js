export const gameInit = {
  viewMode: 'default',
  stepInterval: 10,
  fps: 1000
}

const numCellsX = 80
const numCellsY = Math.round(numCellsX * 0.55)
export const environmentInit = [
  numCellsX,
  numCellsY,
  Math.max(1, Math.floor(900 / numCellsX)),
  Math.max(1, Math.floor(900 / numCellsX)),

  800, // seasonSteps
  { x: 0, y: 0.02 } // gravityVector - probability to fall down on one pixel
]

export const botGeneratorInit = [
  numCellsY * numCellsX / 50,
  numCellsX,
  numCellsY,
  0.5,
  {
    light: 0.5,
    temperature: 0.5
  },
  {
    minerals: 0.5
  },
  true
]

export const stats = {
  maxAge: 0,
  maxGeneration: 0,
  maxMutations: 0,
  died: {
    total: 0,
    health: 0,
    resources: 0,
    age: 0,
    attack: 0,
    attacking: 0,
    move: 0,
    step: 0,
    breed: 0
  },
  diedAvg: {
    total: 0,
    health: 0,
    resources: 0,
    age: 0,
    attack: 0,
    attacking: 0,
    grow: 0
  },
  stepTime: 0,
  gameRunTime: 0,
  animationRunTime: 0,
  gameRunStepCount: 0,
  animationRunStepCount: 0,
  framePerSecond: 0,
  stepPerSecond: 0
}

export const CONST_CELL = {
  RECOVERY_SPEED: {
    light: 0.5,
    temperature: 0.1,
    minerals: 0.01
  }
}

export const CONST_BOT = {
  MUTATION_PROBABILITY: 0.2,
  MUTATION_AMOUNT: 0.02,
  HEALTH_RECOVERY: {
    PRIORITY: { minerals: 1 }
  },
  HEALTH_COST: {
    STEP: 0.001,
    MOVE: 0.001,
    CHANGE_DIRECTION: 0.01,
    GROW: 0.001
  },
  RESOURCE_LIMITS: {
    light: [null, null],
    temperature: [null, 0.95],
    minerals: [null, null]
  },
  INITIAL_SIZE: 0.34, // Relative to sizeMax. Initial size on born
  MIN_SIZE: 0.3, // Relative to genome value. To prevent zero size
  DRAW_SIZE: 1, // How to draw
  GROW_SPEED: 0.33, // Relative to sizeMax. Grow eacj step on GROW_SPEED
  MIN_SPEED: 0.3, // Relative to size. To prevent zero speed
  SPEED_SLOWDOWN: 0.5, // Relative to genome * size. Speed ratio
  MAX_CHANGE_DIRECTION_ANGLE: 0.01,
  MAX_BOTS: 500,
  MAX_AGE: 1600,
  MAX_LEAVE_RESOURCES_COUNTER: 100,
  MAX_DESTRUCTION_COUNTER: 100
}

export const COLORS = {
  displayBackground: 0x151919,
  resource: 0x00ff00,
  cell: 0x077e94,
  minerals: 0x079465, // 66cc99
  selectedInnerBorder: 0x992313,
  selectedOuterBorder: 0xe5a339
}

export const debug = {
  trace: false,
  resource: '',
  breed: false,
  die: false,
  grid: false
}
