export default function Color (intColor = 0) {
  this.color = intColor

  this.toHexStr = function () {
    if (this.color !== null) {
      return '#' + ('00000' + (this.color).toString(16)).substr(-6)
    } else {
      return 'white'
    }
  }

  this.toRgb = function (color) {
    if (color === undefined) {
      color = this.color
    } else if (color instanceof Color) {
      color = color.color
    } else if (color instanceof Object) {
      return color
    }
    const r = color >> 16
    const g = color >> 8 & 0xFF
    const b = color & 0xFF
    return { r, g, b }
  }

  this.fromRgb = function (rgb) {
    this.color = rgb.r << 16 | rgb.g << 8 | rgb.b
    return this
  }

  this.desaturate = function (amount = 0) {
    const rgb = this.toRgb()
    const gray = (rgb.r + rgb.g + rgb.b) / 3
    const grayRgb = {
      r: gray,
      g: gray,
      b: gray
    }
    return this.mix(grayRgb, amount)
  }

  this.darken = function (amount = 0) {
    const black = 0
    return this.mix(black, amount)
  }

  this.brighten = function (amount = 0) {
    amount = amount < 0 ? 0 : amount > 1 ? 1 : amount
    const rgb = this.toRgb()
    // const max = (rgb.r + rgb.g + rgb.b) / 3;
    const max = Math.max(rgb.r, rgb.g, rgb.b)
    rgb.r = Math.max(0, Math.min(255, rgb.r + (255 - max) * amount))
    rgb.g = Math.max(0, Math.min(255, rgb.g + (255 - max) * amount))
    rgb.b = Math.max(0, Math.min(255, rgb.b + (255 - max) * amount))
    return this.fromRgb(rgb)
  }

  this.mix = function (color, amount = 0.5) {
    amount = amount < 0 ? 0 : amount > 1 ? 1 : amount
    const rgb1 = this.toRgb()
    const rgb2 = this.toRgb(color)

    const rgb = {
      r: ((rgb2.r - rgb1.r) * amount) + rgb1.r,
      g: ((rgb2.g - rgb1.g) * amount) + rgb1.g,
      b: ((rgb2.b - rgb1.b) * amount) + rgb1.b
    }

    return this.fromRgb(rgb)
  }

  this.spin = function (amount = 0) {
    const hsl = this.rgbToHsl(this.toRgb())
    const hue = (hsl.h + amount) % 1.0
    hsl.h = hue < 0 ? 1 + hue : hue
    return this.fromRgb(this.hslToRgb(hsl.h, hsl.s, hsl.l))
  }

  // from tinycolor
  this.rgbToHsl = function (rgb) {
    rgb.r = bound01(rgb.r, 255)
    rgb.g = bound01(rgb.g, 255)
    rgb.b = bound01(rgb.b, 255)

    const max = Math.max(rgb.r, rgb.g, rgb.b)
    const min = Math.min(rgb.r, rgb.g, rgb.b)
    let h = (max + min) / 2
    let s = h
    let l = h

    if (max === min) {
      h = s = 0 // achromatic
    } else {
      const d = max - min
      s = l > 0.5 ? d / (2 - max - min) : d / (max + min)
      switch (max) {
        case rgb.r: h = (rgb.g - rgb.b) / d + (rgb.g < rgb.b ? 6 : 0); break
        case rgb.g: h = (rgb.b - rgb.r) / d + 2; break
        case rgb.b: h = (rgb.r - rgb.g) / d + 4; break
      }

      h /= 6
    }

    return { h: h, s: s, l: l }
  }

  // from tinycolor
  this.hslToRgb = function (h, s, l) {
    let r
    let g
    let b

    function hue2rgb (p, q, t) {
      if (t < 0) t += 1
      if (t > 1) t -= 1
      if (t < 1 / 6) return p + (q - p) * 6 * t
      if (t < 1 / 2) return q
      if (t < 2 / 3) return p + (q - p) * (2 / 3 - t) * 6
      return p
    }

    if (s === 0) {
      r = g = b = l // achromatic
    } else {
      const q = l < 0.5 ? l * (1 + s) : l + s - l * s
      const p = 2 * l - q
      r = hue2rgb(p, q, h + 1 / 3)
      g = hue2rgb(p, q, h)
      b = hue2rgb(p, q, h - 1 / 3)
    }

    return { r: r * 255, g: g * 255, b: b * 255 }
  }

  function bound01 (n, max) {
    // Handle floating point rounding errors
    if ((Math.abs(n - max) < 0.000001)) {
      return 1
    }

    // Convert into [0, 1] range if it isn't already
    return (n % max) / parseFloat(max)
  }
}
