import Cell from './Cell'

export default class Environment {
  constructor (numCellsX, numCellsY, cellHeight, cellWidth, seasonSteps, gravityVector) {
    this.numCellsX = numCellsX
    this.numCellsY = numCellsY
    this.cellHeight = cellHeight
    this.cellWidth = cellWidth

    this.seasonSteps = seasonSteps
    this.gravityVector = gravityVector

    this.cells = []
    this.init()
  }

  init () {
    for (let y = 0; y < this.numCellsY; y++) {
      this.cells[y] = []
      for (let x = 0; x < this.numCellsX; x++) {
        this.cells[y][x] = new Cell(this, x, y)
      }
    }
  }

  step (game) {
    for (let y = 0; y < this.numCellsY; y++) {
      for (let x = 0; x < this.numCellsX; x++) {
        this.cells[y][x].step()
      }
    }
  }

  draw (display, viewMode) {
    for (let y = 0; y < this.numCellsY; y++) {
      for (let x = 0; x < this.numCellsX; x++) {
        const cell = this.cells[y][x]
        cell.draw(display, viewMode)
      }
    }
  }

  coordToCellIndex (x, y) {
    return {
      x: Math.floor(x),
      y: Math.floor(y)
    }
  }

  boundX (x, y) {
    const maxX = this.numCellsX
    if (x >= maxX) {
      x = x % maxX
    } else if (x < 0) {
      x = maxX + x % maxX
    }
    return x
  }

  boundY (x, y) {
    const maxY = this.numCellsY
    if (y >= maxY) {
      y = maxY - 0.001
    } else if (y < 0) {
      y = 0
    }
    return y
  }

  boundXY (x, y) {
    return {
      x: this.boundX(x, y),
      y: this.boundY(x, y)
    }
  }

  getCellForCoords (x, y) {
    const cellIndex = this.coordToCellIndex(x, y)
    return this.cells[cellIndex.y][cellIndex.x]
  }

  getSeasonName (stepNumber) {
    if (this.seasonSteps === null) {
      return ''
    }
    const seasonName = ['summer', 'autumn', 'winter', 'spring']
    const season = Math.floor(((stepNumber + this.seasonSteps / 8) % this.seasonSteps) / this.seasonSteps * 4)
    return seasonName[season]
  }

  changeResource (x, y, resource, delta) {
    let actualDelta = 0
    const cell = this.getCellForCoords(x, y)
    if (resource === 'light' && delta < 0) {
      return Math.max(-cell.resources[resource], delta)
    }
    if (delta < 0) {
      delta = Math.max(delta, -cell.resources[resource])
    } else {
      delta = Math.min(delta, 1 - cell.resources[resource])
    }

    for (let offsetX = -1; offsetX <= 1; offsetX++) {
      for (let offsetY = -1; offsetY <= 1; offsetY++) {
        if (offsetX === 0 && offsetY === 0) {
          continue
        }
        const cellCoord = this.boundXY(x + offsetX, y + offsetY)
        const boundCell = this.getCellForCoords(cellCoord.x, cellCoord.y)
        const piece = (offsetX === 0 || offsetY === 0) ? 0.125 : 0.0625
        if (boundCell.resources[resource] >= piece) {
          actualDelta += boundCell.changeResource(resource, delta * piece)
        }
      }
    }
    actualDelta += cell.changeResource(resource, delta - actualDelta)
    return actualDelta
  }
}
