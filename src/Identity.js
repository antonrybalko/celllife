import { debug, CONST_BOT } from './globals'

export const CONST_IDENTITY = {
  COLOR: 0,
  MOVE_SPEED: 1,
  AGING_SPEED: 2,
  BREED_TYPE: 3,
  ATTACK: 4,
  MOVE_DIRECTION_CHANGE: 5,
  SIZE: 6,
  GROW: 7,

  EAT: 10,
  EAT_LIGHT: 11,
  EAT_TEMP: 12,
  EAT_MINERALS: 13,

  REC: 20,
  REC_LIGHT: 21,
  REC_TEMP: 22,
  REC_MINERALS: 23,

  GEN: 30,
  GEN_LIGHT: 31,
  GEN_TEMP: 32,
  GEN_MINERALS: 33,

  BREED_READY: 40,
  BREED_READY_LIGHT: 41,
  BREED_READY_TEMP: 42,
  BREED_READY_MINERALS: 43,
  BREED_READY_HEALTH: 49,

  BREED_GIVE: 50,
  BREED_GIVE_LIGHT: 51,
  BREED_GIVE_TEMP: 52,
  BREED_GIVE_MINERALS: 53,
  BREED_GIVE_HEALTH: 59,

  BODY_POINTS: 60,

  OPERATIONS: 70,

  MAX: 200
}

export default function Identity (identity = null, countGenMutation = CONST_IDENTITY.COLOR) {
  this.mutationCount = 0
  this.genMutationCount = 0 // number of mutations since specific gen mutation (countGenMutation)
  this.genome = {}
  this.mutationProbability = CONST_BOT.MUTATION_PROBABILITY
  this.countGenMutation = countGenMutation // specify gen to count relative mutations count
  this.surname = ''
  this.surnameColor = null

  this.clone = function (target, source) {
    // Fast clone. To try for..of
    target.genome = source.genome.slice()
    target.mutationProbability = source.mutationProbability
    target.mutationCount = source.mutationCount
    target.countGenMutation = source.countGenMutation
    target.genMutationCount = source.genMutationCount
    target.surname = identity.getSurname()
    target.surnameColor = identity.getSurnameColor()
  }

  this.init = function (identity) {
    if (Array.isArray(identity)) {
      this.genome = identity.slice()
    } else if (identity instanceof Identity) {
      this.clone(this, identity)
    } else {
      this.initRandom()
    }
  }

  this.initRandom = function () {
    this.genome = new Array(CONST_IDENTITY.MAX)
    var i = this.genome.length
    while (i--) {
      this.genome[i] = Math.random()
    }
  }

  this.getGene = function (geneIndex) {
    if (this.genome.length === 0) {
      return null
    }
    geneIndex = Math.abs(geneIndex % (this.genome.length - 1))
    return this.genome[geneIndex]
  }

  this.getGeneNormalized = function (geneIndex) {
    return Math.abs(this.getGene(geneIndex) % 1)
  }

  this.replicate = function (father) {
    var newIdentity = new Identity(father)

    // copy half from myself
    if (this !== father) {
      var i = parseInt(this.genome.length / 2)
      while (i !== this.genome.length) {
        newIdentity.genome[i] = this.genome[i]
        i++
      }

      // if (this.genome[CONST_IDENTITY.COLOR] !== father.genome[CONST_IDENTITY.COLOR]) {
      // newIdentity.genome[CONST_IDENTITY.COLOR] = 0x000000;
      // newIdentity.genome[CONST_IDENTITY.COLOR] = (new Color(this.getColor()).mix(father.getColor()).color)/0xffffff;
      // }
    }

    // mutation
    if (Math.random() <= this.mutationProbability) {
      for (let i = 0; i < newIdentity.genome.length * CONST_BOT.MUTATION_AMOUNT; i++) {
        var mutationIndex = Math.floor(Math.random() * newIdentity.genome.length)
        newIdentity.genome[mutationIndex] += (Math.random() - 0.5) * 2
        newIdentity.mutationCount += 1
        this.genMutationCount += 1
        if (mutationIndex === this.countGenMutation) {
          this.genMutationCount = 0
          this.surname = ''
          this.surnameColor = null
        }
        if (debug.breed) {
          console.log('mutation', mutationIndex)
        }
      }
    }
    return newIdentity
  }

  this.partnerCompatible = function (partnerIdentity) {
    if (this.genome.length !== partnerIdentity.genome.length) {
      return false
    }
    if (this.genome[0] !== partnerIdentity.genome[0] && this.genome[1] !== partnerIdentity.genome[1] && this.genome[2] !== partnerIdentity.genome[2]) {
      return false
    }
    var difference = 0
    const len = this.genome.length / 2
    for (let i = 0; i < len; i++) {
      difference += this.genome[i] !== partnerIdentity.genome[i] ? 1 : 0
    }
    return difference !== 0 && difference !== this.genome.length / 2
  }

  this.canSexualReproduction = function () {
    return this.genome[CONST_IDENTITY.BREED_TYPE] >= 0.5
  }

  this.getColor = function () {
    const color = (this.floatColorToInt(this.genome[CONST_IDENTITY.COLOR], 0xff) << 16) +
      (this.floatColorToInt(this.genome[CONST_IDENTITY.COLOR + 1], 0xff) << 8) +
      (this.floatColorToInt(this.genome[CONST_IDENTITY.COLOR + 2], 0xff))
    return color
  }

  this.floatColorToInt = function (floatColor, maxInt = 0xffffff) {
    return parseInt(Math.abs(floatColor % 1) * maxInt)
  }

  this.getSurnameColor = function () {
    if (this.surnameColor !== null) {
      return this.surnameColor
    }
    return this.getColor()
  }

  this.getName = function () {
    return botNames[parseInt(this.getGeneNormalized(CONST_IDENTITY.COLOR) * 100 || 0)]
  }

  this.getSurname = function () {
    if (this.surname !== '') {
      return this.surname
    }
    return botSurnames[parseInt(this.getGeneNormalized(CONST_IDENTITY.COLOR) * 100 || 0)]
  }

  this.getFullName = function () {
    return this.getName() + ' ' + this.genMutationCount + ' ' + this.getSurname()
  }

  this.init(identity)
}

var botNames = [
  'Howard',
  'Nicholson',
  'Ingram',
  'Concepcion',
  'Francis',
  'Kay',
  'Hicks',
  'Richards',
  'Walls',
  'Stacey',
  'Delores',
  'Spencer',
  'Wright',
  'Keith',
  'Craft',
  'Marquez',
  'Rocha',
  'Cain',
  'Elliott',
  'Regina',
  'Dunn',
  'Jan',
  'Fry',
  'Cynthia',
  'Candace',
  'Sargent',
  'Vinson',
  'Gould',
  'Shirley',
  'Gay',
  'Snider',
  'Guadalupe',
  'Claudia',
  'Lori',
  'Hurley',
  'Priscilla',
  'Mcdowell',
  'Durham',
  'Vilma',
  'Alice',
  'Mejia',
  'Bullock',
  'Jana',
  'Daisy',
  'Shauna',
  'Everett',
  'Wise',
  'Aguilar',
  'Gardner',
  'Janie',
  'Edwina',
  'Melissa',
  'Michelle',
  'Fran',
  'Meredith',
  'Mona',
  'Chris',
  'Molina',
  'Tessa',
  'Kinney',
  'Hebert',
  'Padilla',
  'Mandy',
  'Christina',
  'Phoebe',
  'Albert',
  'Baird',
  'Mosley',
  'Gillespie',
  'Thomas',
  'Riggs',
  'Deloris',
  'Belinda',
  'Henrietta',
  'Marsh',
  'Rosalinda',
  'Sanders',
  'Walter',
  'Dorthy',
  'Callie',
  'Charity',
  'Chase',
  'Millicent',
  'Dalton',
  'Coleen',
  'Walton',
  'Herman',
  'Angeline',
  'Wilkinson',
  'Odonnell',
  'Maureen',
  'Jami',
  'Ellis',
  'Janell',
  'Ortega',
  'Puckett',
  'Sexton',
  'Lina',
  'Vaughn',
  'Dickerson'
]

var botSurnames = [
  'Sullivan',
  'Benjamin',
  'Chandler',
  'Lloyd',
  'Robinson',
  'Sharpe',
  'Frazier',
  'Moreno',
  'Sweeney',
  'Madden',
  'Wolf',
  'Nixon',
  'Lowe',
  'Griffith',
  'Collins',
  'Watson',
  'Hughes',
  'Edwards',
  'Mccarthy',
  'Mccullough',
  'Velez',
  'Small',
  'Gentry',
  'Farley',
  'Chen',
  'Hale',
  'Weeks',
  'Schultz',
  'Skinner',
  'Pickett',
  'Bishop',
  'Guthrie',
  'Watts',
  'Slater',
  'Sykes',
  'Shields',
  'Hansen',
  'Salas',
  'Meyers',
  'Thornton',
  'Zamora',
  'Fischer',
  'Riley',
  'Todd',
  'Mcgee',
  'Haley',
  'Estrada',
  'Whitney',
  'Crane',
  'Cervantes',
  'Randall',
  'Coffey',
  'Trujillo',
  'Roy',
  'Allison',
  'Richmond',
  'Cooley',
  'House',
  'Norris',
  'Massey',
  'Raymond',
  'Sandoval',
  'Sanchez',
  'Garner',
  'Barnes',
  'Moses',
  'Booker',
  'Carey',
  'Mcclain',
  'Lambert',
  'Burke',
  'Perkins',
  'Reid',
  'Mitchell',
  'Foster',
  'Donaldson',
  'Strickland',
  'Valdez',
  'Montgomery',
  'Bullock',
  'Hayes',
  'Cameron',
  'Farrell',
  'Trevino',
  'Berg',
  'Marsh',
  'Crosby',
  'Rush',
  'Wiggins',
  'Stanley',
  'Byers',
  'Boyer',
  'Romero',
  'Elliott',
  'Harris',
  'Bowers',
  'Clarke',
  'Hanson',
  'Cole',
  'Morrison'
]
