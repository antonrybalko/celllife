import { stats, debug, CONST_BOT, COLORS } from './globals'
import { CONST_IDENTITY } from './Identity'
import Color from './Color'

export default class BotState {
  constructor (game, cell, x, y, direction, identity, health, factors, resources) {
    const name = identity.getFullName()

    this.type = 'bot'
    this.alive = true
    this.health = health
    this.dieReason = 'not yet'
    this.destructed = false
    this.x = x
    this.y = y
    this.node = null
    this.stats = {
      age: 0,
      generation: 0,
      breedings: 0
    }
    this.factors = { ...factors }
    this.resources = { ...resources }
    this.currentProduction = { light: 0, minerals: 0, temperature: 0 }
    this.currentConsumption = { light: 0, minerals: 0, temperature: 0 }
    this.currentTransformation = { light: 0, minerals: 0, temperature: 0 }
    this.currentRecover = { health, light: 0, minerals: 0, temperature: 0 }
    this.identity = identity
    this.game = game
    this.env = game.env
    this.cell = cell
    this.cellNode = null // save cell content position
    this.name = name
    this.resourceNames = ['minerals']
    this.isSelected = false
    this.destructionCounter = 0
    this.sizeMax = CONST_BOT.MIN_SIZE + identity.getGeneNormalized(CONST_IDENTITY.SIZE) * (1 - CONST_BOT.MIN_SIZE)
    this.size = CONST_BOT.INITIAL_SIZE * this.sizeMax
    this.moveSpeed = 0
    this.moveDirection = direction
    this.bodyPoints = this.constructBodyPoints()
    this.prev = {
      cantMove: 0,
      isBreeding: 0,
      cantBreed: 0,
      isAttacking: 0,
      isUnderAttack: 0,
      isEatingBot: 0
    }
    this.states = []

    if (game) {
      this.node = game.addBot(this)
      this.game.botsAlive += 1
    }
    if (debug.breed) {
      console.log('new bot', name, game ? game.botsAlive : null)
    }
  }

  getProps () {
    const minerals = this.resources.minerals
    return {
      light: this.currentProduction.light,
      minerals: minerals,
      temperature: this.factors.temperature,
      opacity: 0.9 * (1 - minerals), // maximum opacity = 90%
      hardness: 1 - 0.9 * minerals // minimum hardness = 10%
    }
  }

  step () {
    if (this.isSelected && debug.trace) {
      console.log('step')
    }
    if (!this.alive) {
      this.stats.age += 1
      this.destruction()
      return false
    }
    this.stateRandomCounter = 0
    this.states = [
      this.stats.age / CONST_BOT.MAX_AGE,
      this.health,
      // this.size,
      this.moveSpeed,
      this.moveDirection,
      this.y / this.env.numCellsY,
      this.factors.light,
      this.factors.temperature,
      this.resources.minerals,
      this.currentConsumption.minerals,
      this.currentProduction.minerals,
      // this.cell.resources.light,
      // this.cell.resources.temperature,
      // this.cell.resources.minerals,
      ...Object.values(this.prev)
    ]
    // current state
    this.states.push(this.isGrowing() ? 1 : 0)
    const neighbourBot = this.lookForBot()
    if (this.isSelected) {
      console.log(neighbourBot && !this.canAttack(neighbourBot))
    }
    this.states.push(neighbourBot && !this.canAttack(neighbourBot) ? 1 : 0) // friend in front of you
    // cell in front of you
    const offset = this.vector(this.moveDirection, this.size * 1.5)
    const coord = this.env.boundXY(this.x + offset.x, this.y + offset.y)
    const neighbourCell = this.env.getCellForCoords(coord.x, coord.y)
    if (neighbourCell && this.cell) {
      this.states.push(neighbourCell.resources.minerals - this.cell.resources.minerals)
      this.states.push(neighbourCell.resources.light - this.cell.resources.light)
    }
    for (const prevKey in this.prev) {
      this.prev[prevKey] = 0
    }
    // always grow if needed
    this.grow()
    this.updateFactors()
    const operations = [
      'breed',
      'attack',
      'consume',
      'transformResources',
      'recoverHealth',
      'produce',
      'changeMoveDirection',
      'move'
    ]
    const operationActivators = this.getOperationActivators(operations)
    operations.map((operation, index) => {
      if (operationActivators[index] >= 0.5) {
        this[operation]()
      }
    })
    this.decreaseHealt(-CONST_BOT.HEALTH_COST.STEP, 'step')
    this.checkResourcesOrDie()
    this.stats.age += 1
    stats.maxAge = Math.max(stats.maxAge, this.stats.age)
  }

  draw (display, viewMode) {
    const pos = this.getCellPos(display)
    const fillColor = this.getFillColor(viewMode)
    if (debug.grid) {
      display.graphics.lineStyle(1, fillColor)
    } else {
      display.graphics.lineStyle(0)
    }
    display.graphics.beginFill(fillColor, this.getFillAlpha(viewMode))
    const w = display.cellWidth / 2
    const h = display.cellHeight / 2
    const polygon = []
    for (const point of [...this.bodyPoints, this.bodyPoints[0]]) {
      const angle = (point.angle + this.moveDirection) * Math.PI * 2
      polygon.push(
        pos.x + w * Math.cos(angle) * point.length * this.size * CONST_BOT.DRAW_SIZE,
        pos.y + h * Math.sin(angle) * point.length * this.size * CONST_BOT.DRAW_SIZE
      )
    }
    display.graphics.drawPolygon(polygon)
    display.graphics.endFill()

    // Draw bot center
    /*
    display.graphics.beginFill(0, 0.25)
    display.graphics.drawRect(
      pos.x - 1,
      pos.y - 1,
      2,
      2
    )
    display.graphics.endFill()
    */

    // Draw bot direction marker

    if (['default', 'env'].includes(viewMode)) {
      display.graphics.beginFill(0, 0.3)
      const lastPoint = this.bodyPoints[this.bodyPoints.length - 1]
      const angle = (lastPoint.angle + this.moveDirection) * Math.PI * 2
      display.graphics.drawEllipse(
        pos.x + w * Math.cos(angle) * (0.5 * lastPoint.length) * this.size * CONST_BOT.DRAW_SIZE,
        pos.y + h * Math.sin(angle) * (0.5 * lastPoint.length) * this.size * CONST_BOT.DRAW_SIZE,
        w * this.size * CONST_BOT.DRAW_SIZE / 4,
        h * this.size * CONST_BOT.DRAW_SIZE / 4
      )
      display.graphics.endFill()
    }
  }

  getCellPos (display) {
    return {
      x: this.x * display.cellWidth,
      y: this.y * display.cellHeight
    }
  }

  getFillColor (viewMode = 'env') {
    let color = COLORS.resource
    if (viewMode === 'default' || viewMode === 'env') {
      if (!this.isUnderAttack) {
        color = new Color(this.getColor()) // no need for spin for three component color gene .spin(this.identity.genMutationCount * 0.001)
      } else {
        color = new Color(0xFF0000)
      }

      if (this.alive) {
        color.desaturate(0.25 * (1 - this.health))
      } else {
        color.desaturate(0.5).darken(0.2)
      }

      if (viewMode === 'env' && this.cell) {
        // what ???
        // color.darken(0.3 * (1 - this.cell.resources.light * this.cell.resources.light))
        const light = this.cell.getProps().light
        if (light > 0.5) {
          color.brighten((light - 0.5) * 1.5)
        } else {
          color.darken((0.5 - light))
        }
      }

      color = color.color
    } else if (viewMode === 'population') {
      color = this.identity.getSurnameColor()
    }
    return color
  }

  getFillAlpha (viewMode = 'env') {
    let alpha = 1
    if (viewMode === 'default' || viewMode === 'env') {
      if (this.alive) {
        alpha = 0.7 + this.resources.minerals * 0.3
      } else {
        alpha = 0.5
      }
    } else if (viewMode === 'health') {
      alpha = this.health
    } else if (viewMode === 'age') {
      alpha = this.stats.age / stats.maxAge
    } else if (viewMode === 'generation') {
      alpha = this.stats.generation / stats.maxGeneration
    } else if (viewMode === 'mutations') {
      alpha = this.identity.genMutationCount / stats.maxMutations
    } else {
      alpha = this.getProps()[viewMode]
    }
    return alpha
  }

  getColor () {
    return this.identity.getColor()
  }

  changeMoveDirection () {
    if (!this.alive) {
      return
    }
    const angle = this.getStateRandom(50) // - 0.25
    // console.log(angle)
    const delta = this.setMoveDirection(this.moveDirection + angle)
    if (this.isSelected) {
      console.log(angle, delta, this.moveDirection)
    }
    this.decreaseHealt(-CONST_BOT.HEALTH_COST.CHANGE_DIRECTION * Math.abs(delta), 'changeMoveDirection')
  }

  setMoveDirection (newValue) {
    newValue = newValue % 1
    if (newValue < 0) {
      newValue = 1 + newValue
    }
    let delta = newValue - this.moveDirection
    if (delta < 0) {
      delta = 1 + delta
    }
    /* if (delta > CONST_BOT.MAX_CHANGE_DIRECTION_ANGLE) {
      delta = CONST_BOT.MAX_CHANGE_DIRECTION_ANGLE
    } */
    delta *= CONST_BOT.MAX_CHANGE_DIRECTION_ANGLE
    this.moveDirection += delta
    this.moveDirection = this.moveDirection % 1
    return delta
  }

  vector (angle, length = 1) {
    angle *= Math.PI * 2
    return {
      x: length * Math.cos(angle),
      y: length * Math.sin(angle)
    }
  }

  move () {
    if (!this.alive) {
      return false
    };

    this.moveSpeed = this.getStateRandom(CONST_IDENTITY.MOVE_SPEED)
    const offset = this.vector(this.moveDirection, this.moveSpeed * (CONST_BOT.MIN_SPEED + (1 - this.size) * CONST_BOT.MIN_SPEED) * CONST_BOT.SPEED_SLOWDOWN * this.factors.temperature)
    const canMove = this.moveBot(offset)
    this.decreaseHealt(-CONST_BOT.HEALTH_COST.MOVE * this.moveSpeed * (1 - this.size), 'move')
    this.cantMove = canMove ? 0 : 1
    return canMove
  }

  moveBot (offset) {
    const y = this.env.boundY(this.x + offset.x, this.y + offset.y)
    const x = this.env.boundX(this.x + offset.x, this.y + offset.y)
    if (isNaN(x) || isNaN(y)) {
      console.log('moveBot aaaaaaaaaaaaa!!!!', this.x, this.y, this)
    }
    if (this.isSelected && debug.trace) {
      console.log(`try to move to x: ${x}, y: ${y}, offset: ${offset.x}, ${offset.y}`)
    }

    if (this.game.getCollisions(this, x, y, offset).length) {
      if (this.isSelected && debug.trace) {
        console.log('cant move')
      }
      return false
    }

    const newCell = this.env.getCellForCoords(x, y)
    if (this.cell && this.cell !== newCell) {
      const newCellNode = newCell.addContent(this)
      this.cell.removeContent(this.cellNode)
      this.cell = newCell
      this.cellNode = newCellNode
    }

    const canMove = (this.x + offset.x === x) && (this.y + offset.y === y)
    this.x = x
    this.y = y
    return canMove
  }

  grow () {
    if (!this.isGrowing()) {
      return
    }

    const delta = Math.abs(this.identity.genome[CONST_IDENTITY.GROW] % 1) * CONST_BOT.GROW_SPEED
    const health = CONST_BOT.HEALTH_COST.GROW * delta
    if (this.health <= health) {
      return
    }

    this.changeHealth(-health, 'grow')

    this.size += delta * this.sizeMax
    if (this.size > this.sizeMax) {
      this.size = this.sizeMax
    }
  }

  breed () {
    const offset = this.vector(this.moveDirection + this.getStateRandom(CONST_IDENTITY.BREED_READY + 1), this.size)
    if (!this.alive || this.isGrowing()) {
      return false
    };

    let y = this.y + offset.y
    let x = this.x + offset.x

    if (y >= this.env.numCellsY || y < 0) {
      return false
    }

    if (x >= this.env.numCellsX) {
      x = 0
    }

    if (x < 0) {
      x = this.env.numCellsX - 1
    }

    const collisions = this.game.getCollisions(this, x, y)
    if (collisions.length > 0) {
      this.cantBreed = 1
      return false
    }

    const newCell = this.env.getCellForCoords(x, y)
    const newBot = this.breedBot(x, y, newCell)
    if (!newBot) {
      return false
    }

    newBot.cellNode = newCell.addContent(newBot)
    this.stats.breedings += 1
    this.isBreeding = 1
    return true
  }

  breedBot (x, y, newCell) {
    if (this.game.bots.length() >= CONST_BOT.MAX_BOTS) {
      return null
    }

    if (debug.breed) {
      console.log('breeding', this.name, this.x, this.y)
      console.log('breed before ', this.health, this.resources)
    }

    // reduce health
    const health = this.health * this.getStateRandom(CONST_IDENTITY.BREED_GIVE_HEALTH)
    this.changeHealth(-health, 'breed')
    // reduce resources
    const newResources = {}
    let i = this.resourceNames.length
    while (i--) {
      newResources[this.resourceNames[i]] = -this.changeResource(this.resourceNames[i], -this.resources[this.resourceNames[i]] * this.getStateRandom(CONST_IDENTITY.BREED_GIVE + i))
    }

    // replicate identity
    const replica = this.replicate()
    const newBot = new BotState(this.game, newCell, x, y, this.moveDirection + this.getStateRandom(CONST_IDENTITY.BREED_READY + 2), replica, health, this.factors, newResources)

    // change stats
    stats.maxMutations = Math.max(stats.maxMutations, newBot.identity.mutationCount)
    newBot.stats.generation = this.stats.generation + 1
    stats.maxGeneration = Math.max(stats.maxGeneration, newBot.stats.generation)
    if (debug.breed) {
      console.log('seed ', newBot.name, newBot.x, newBot.y)
      console.log('breed new ', newBot.health, newBot.resources)
      console.log('breed after ', this.health, this.resources)
    }
    return newBot
  }

  replicate () {
    if (this.identity.canSexualReproduction()) {
      const partner = this.lookForBot()
      if (partner !== null && this.identity.partnerCompatible(partner.identity)) {
        return partner.identity.replicate(this.identity)
      }
    }
    // asexual
    return this.identity.replicate(this.identity)
  }

  lookForBot (aliveOnly = true) {
    const offset = this.vector(this.moveDirection, this.size * 0.8)
    const coord = this.env.boundXY(this.x + offset.x, this.y + offset.y)
    const collisions = this.game.getCollisions(this, coord.x, coord.y)
    if (collisions.length === 0) {
      return null
    }
    for (const bot of collisions) {
      if (bot !== this && (!aliveOnly || bot.alive)) {
        return bot
      }
    }
    return null
  }

  attack () {
    if (!this.alive) {
      return false
    }

    //  look for victim
    let bot = this.lookForBot(false)
    if (bot !== null && (!bot.alive || this.canAttack(bot))) {
      this.attackBot(bot)
    }
  }

  canAttack (bot) {
    // dont attack similar color
    const cantAttack = Math.abs(bot.identity.genome[CONST_IDENTITY.COLOR] - this.identity.genome[CONST_IDENTITY.COLOR]) < 0.25 &&
      Math.abs(bot.identity.genome[CONST_IDENTITY.COLOR + 1] - this.identity.genome[CONST_IDENTITY.COLOR + 1]) < 0.25 &&
      Math.abs(bot.identity.genome[CONST_IDENTITY.COLOR + 2] - this.identity.genome[CONST_IDENTITY.COLOR + 2]) < 0.25
    if (this.isSelected && debug.resource === 'health') {
      console.log('can attack?', !cantAttack)
      console.log('me', this.name, this.x, this.y, this.identity.genome[CONST_IDENTITY.COLOR], this.identity.genome[CONST_IDENTITY.COLOR + 1], this.identity.genome[CONST_IDENTITY.COLOR + 2])
      console.log('enemy', bot.name, bot.x, bot.y, bot.identity.genome[CONST_IDENTITY.COLOR], bot.identity.genome[CONST_IDENTITY.COLOR + 1], bot.identity.genome[CONST_IDENTITY.COLOR + 2])
    }
    return !cantAttack
    // attack bot with different color
    // return bot.identity.genome[CONST_IDENTITY.COLOR] !== this.identity.genome[CONST_IDENTITY.COLOR] &&
    //   bot.identity.genome[CONST_IDENTITY.BODY_POINTS] !== this.identity.genome[CONST_IDENTITY.BODY_POINTS]
  }

  attackBot (bot) {
    if (!bot.alive) {
      this.eatBot(bot)
      return
    }
    const attack = -this.getAttackRatio()
    this.changeHealth(attack * 0.5, 'attacking')
    bot.changeHealth(attack * (1 - bot.getDefenceRatio()), 'attack')
    this.isAttacking = 1
    bot.underAttack = 1
  }

  eatBot (bot) {
    let ratio = this.size / bot.size
    const takenResources = -bot.changeResource('minerals', -bot.resources.minerals * ratio)
    this.changeResource('minerals', takenResources)
    this.factors.temperature = (bot.factors.temperature + this.factors.temperature) / 2
    if (ratio < 1) {
      bot.size = Math.max(0.1, bot.size * (1 - ratio))
    } else {
      bot.destruct()
    }
    this.isEatingBot = 1
  }

  getAttackRatio () {
    return this.getStateRandom(CONST_IDENTITY.ATTACK) * this.size * this.health
  }

  getDefenceRatio () {
    return this.resources.minerals * this.size
  }

  consume () {
    if (!this.alive) {
      return
    };

    let i = this.resourceNames.length
    while (i--) {
      this.currentConsumption[this.resourceNames[i]] = 0
      this.consumeResource(this.resourceNames[i], this.getStateRandom(CONST_IDENTITY.EAT + i) * this.size * this.factors.temperature)
    }
  }

  updateFactors () {
    // light
    this.factors.light = this.cell.resources.light

    // temperature
    const random = this.getStateRandom(CONST_IDENTITY.EAT_TEMP)
    if (this.factors.temperature === undefined) {
      console.log('this.factors.temperature')
    }
    if (this.cell.resources.temperature === undefined) {
      console.log('this.cell.resources.temperature')
    }
    if (isNaN(random)) {
      console.log('random')
    }
    const delta = -this.env.changeResource(this.x, this.y, 'temperature', random * (this.factors.temperature - this.cell.resources.temperature) / 2)
    this.factors.temperature = Math.max(0, Math.min(1, this.factors.temperature + delta))
    if (isNaN(this.factors.temperature)) {
      console.log('temperature 2222222222222222222222 ', this.x, this.y)
    }
  }

  consumeResource (resource, amount) {
    if (this.isSelected && debug.resource && resource === debug.resource) {
      console.log('bot.eat', -amount)
    }
    const offset = this.vector(this.moveDirection, this.size * 1.5)
    const coord = this.env.boundXY(this.x + offset.x, this.y + offset.y)
    const actualDelta = this.changeResource(resource, -this.env.changeResource(coord.x, coord.y, resource, -amount))
    this.currentConsumption[resource] = actualDelta
    return actualDelta
  }

  transformResources () {
    if (!this.alive) {
      return false
    };

    // let i = this.resourceNames.length
    // while (i--) {
    //   this.currentTransformation[this.resourceNames[i]] = 0
    // }
    // const delta = this.changeResource('temperature', this.currentConsumption.minerals * this.currentConsumption.light * this.getStateRandom(CONST_IDENTITY.GEN_TEMP))
    // if (delta !== 0) {
    //   this.currentTransformation.temperature = delta
    //   this.currentTransformation.minerals = this.changeResource('minerals', -delta * this.currentConsumption.minerals / (this.currentConsumption.minerals + this.currentConsumption.light))
    //   this.currentTransformation.light = this.changeResource('light', -delta * this.currentConsumption.light / (this.currentConsumption.minerals + this.currentConsumption.light))
    // }
  }

  produce () {
    if (!this.alive) {
      return
    };

    let i = this.resourceNames.length
    while (i--) {
      this.currentProduction[this.resourceNames[i]] = 0
      this.produceResource(this.resourceNames[i], this.getStateRandom(CONST_IDENTITY.GEN + i) * this.size * this.factors.temperature)
    }
  }

  produceResource (resource, generateRatio) {
    if (this.isSelected && debug.resource && resource === debug.resource) {
      console.log('bot.generate current=', this.resources[resource])
    }
    let cellDelta = 0

    cellDelta = -this.changeResource(resource, -generateRatio)

    if (cellDelta !== 0) {
      const offset = this.vector(-this.moveDirection, this.size * 1.5)
      const coord = this.env.boundXY(this.x + offset.x, this.y + offset.y)
      this.env.changeResource(coord.x, coord.y, resource, cellDelta)
      this.currentProduction[resource] = cellDelta
    }
    if (this.isSelected && debug.resource && resource === debug.resource) {
      console.log('bot.generate end current=', this.resources[resource], cellDelta)
    }
  }

  changeResource (resource, delta) {
    let newValue = this.resources[resource] + delta
    if (newValue > 1) {
      newValue = 1
    }
    if (newValue < 0) {
      newValue = 0
    }
    const actualDelta = newValue - this.resources[resource]
    this.resources[resource] = newValue
    return actualDelta
  }

  decreaseHealt (delta, action) {
    if (!this.alive) {
      return
    };
    this.changeHealth(delta, action)
  }

  recoverHealth () {
    // fill with zero
    this.currentRecover.health = 0
    let i = this.resourceNames.length
    while (i--) this.currentRecover[this.resourceNames[i]] = 0

    if (!this.alive) {
      return 0
    };

    // check bot have enough resources
    // i = this.resourceNames.length
    // while (i--) {
    //   if (this.resources[this.resourceNames[i]] < this.identity.genome[CONST_IDENTITY.REC + i] * this.size) {
    //     return 0
    //   }
    // }

    // calculate health recover
    let deltaHealth = 0
    let sum = 0
    i = this.resourceNames.length
    while (i--) {
      const resource = this.resourceNames[i]
      const recoverDelta = -this.changeResource(resource, -this.getStateRandom(CONST_IDENTITY.REC + i) * this.size)
      sum += recoverDelta * CONST_BOT.HEALTH_RECOVERY.PRIORITY[resource] * this.factors.temperature * this.factors.light
      this.currentRecover[resource] = recoverDelta
    }
    deltaHealth = sum

    // assign new health and return actual delta
    this.changeHealth(deltaHealth, 'recover')
    this.currentRecover.health = deltaHealth
  }

  changeHealth (delta, reason) {
    if (this.health <= 0) {
      return 0
    }
    let newHealth = Math.max(0, Math.min(1, this.health + delta))
    let actualDelta = this.health - newHealth
    this.health = newHealth
    if (this.health <= 0) {
      this.die(reason)
      stats.died.health += 1
    }
    return actualDelta
  }

  checkResourcesOrDie () {
    if (!this.alive) {
      return
    };

    let i = this.resourceNames.length
    while (i--) {
      const resource = this.resourceNames[i]
      if ((CONST_BOT.RESOURCE_LIMITS[resource][0] !== null && this.resources[resource] < CONST_BOT.RESOURCE_LIMITS[resource][0]) ||
          (CONST_BOT.RESOURCE_LIMITS[resource][1] !== null && this.resources[resource] > CONST_BOT.RESOURCE_LIMITS[resource][1])) {
        this.die('resources')
        return
      }
    }
    if (this.alive && this.stats.age >= this.identity.getGeneNormalized(CONST_IDENTITY.AGING_SPEED) * CONST_BOT.MAX_AGE) {
      this.die('age')
    }
  }

  die (reason, instant = false) {
    if (!this.alive) {
      return
    };

    if (debug.die) {
      console.log('die by', reason, this.name, this.x, this.y, this.game.botsAlive)
    }

    this.alive = false
    this.dieReason = reason
    this.health = 0

    stats.died.total += 1
    stats.died[reason] += 1
    this.game.botsAlive -= 1

    if (!instant) {
      this.destruction()
    } else {
      this.destruct()
    }
  }

  leaveResources () {
    let i = this.resourceNames.length
    while (i--) {
      const resource = this.resourceNames[i]
      this.env.changeResource(this.x, this.y, resource, this.resources[resource])
    }
  }

  destruction () {
    if (this.isSelected && debug.trace) {
      console.log('destruction')
    }

    if (this.destructed) {
      return
    }

    if (this.destructionCounter >= CONST_BOT.MAX_DESTRUCTION_COUNTER) {
      this.leaveResources()
      this.destruct()
    } else if (this.resources.minerals <= 0.001) {
      this.destructionCounter += 1
    } else if (this.cell) {
      const leaveMinerals = this.resources.minerals / CONST_BOT.MAX_LEAVE_RESOURCES_COUNTER
      this.resources.minerals -= this.env.changeResource(this.x, this.y, 'minerals', Math.min(leaveMinerals, this.resources.minerals))

      const leaveTemperature = (this.factors.temperature - this.cell.resources.temperature) / 10
      this.factors.temperature -= this.env.changeResource(this.x, this.y, 'temperature', leaveTemperature)
    }
  }

  destruct () {
    this.destructed = true
    this.game.removeBot(this.node)
    if (this.cell) {
      this.cell.removeContent(this.cellNode)
    }
    this.cell = null
  }

  constructBodyPoints () {
    // TODO: move min_points and max_points to globals
    const MIN_POINTS = 3
    const MAX_POINTS = 10
    const LENGTH_MIN = 0.6
    const number = MIN_POINTS + Math.round((MAX_POINTS - MIN_POINTS) * this.identity.getGeneNormalized(CONST_IDENTITY.BODY_POINTS))
    const points = []
    let angleSum = 0
    for (let i = 1; i <= number; i++) {
      const angle = 1 / number // this.identity.getGene(CONST_IDENTITY.BODY_POINTS + i * 2)
      const length = LENGTH_MIN + (1 - LENGTH_MIN) * this.identity.getGeneNormalized(CONST_IDENTITY.BODY_POINTS + i)
      points.push({
        angle,
        length
      })
      angleSum += angle
    }
    for (let i = 0; i < points.length; i++) {
      points[i].angle = points[i].angle / angleSum
      if (i > 0) {
        points[i].angle += points[i - 1].angle
      }
    }
    return points
  }

  getRadius () {
    return this.size / 2 // size is in cell width units
  }

  isGrowing () {
    return this.size < this.sizeMax
  }

  getOperationActivators (operations) {
    let activators = []
    for (let operationIndex = 0; operationIndex < operations.length; operationIndex++) {
      activators.push(this.getStateRandom(operationIndex))
    }
    return activators
  }

  predict (genomeIndex) {
    let result = 0
    let stateIndex = 0
    for (const state of this.states) {
      const currentGenomeIndex = (genomeIndex + stateIndex) % CONST_IDENTITY.MAX
      const genValue = state * (this.identity.getGene(currentGenomeIndex) - 0.5) // gonome - weight
      result += genValue
      // if (this.isSelected) {
      //   console.log('value', genValue, state, this.identity.getGene(currentGenomeIndex), result)
      // }
      stateIndex += 1
    }
    // const genValue = (this.identity.getGene(genomeIndex + this.states.length) - 0.5) // bias
    // result += genValue
    // if (this.isSelected) {
    //   console.log('value', genValue, result)
    // }
    // if (this.isSelected) {
    //   console.log('result', result)
    // }
    // result = this.activate(result, 2 /* 2 * 3.14, 1.5 * 3.14 /* 5 this.states.length */)
    result = this.activate(result, 2 * 3.14, 1.5 * 3.14 /* 5 this.states.length */)
    return result
  }

  activate (value, multiplier = 1, bias = 0) {
    // return 1 / (1 + Math.exp(-value * multiplier + bias))
    return 0.5 + 0.5 * Math.sin(value * multiplier + bias) // multiplier = 2 * 3.14  bias = 2 * 3.14
  }

  getStateRandom (index = null) {
    if (index === null) {
      index = this.stateRandomCounter
      this.stateRandomCounter += 1
    }
    return this.predict(parseInt(this.identity.getGene(CONST_IDENTITY.OPERATIONS + index)) * CONST_IDENTITY.MAX)
  }
}
