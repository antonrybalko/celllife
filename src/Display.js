import { COLORS } from './globals'
var PIXI = require('pixi.js')

export default class Display {
  constructor (canvasId, numCellsX, numCellsY, cellWidth, cellHeight) {
    this.numCellsX = numCellsX
    this.numCellsY = numCellsY
    this.cellWidth = cellWidth
    this.cellHeight = cellHeight
    this.width = numCellsX * cellWidth
    this.height = numCellsY * cellHeight
    this.stickSelected = false
    this.app = new PIXI.Application(
      this.width,
      this.height,
      {
        antialias: true, // works only for triangles?
        view: document.getElementById(canvasId)
      }
    )
    this.app.renderer.backgroundColor = COLORS.displayBackground
    this.app.renderer.autoResize = false
    this.graphics = new PIXI.Graphics()
    this.app.stage.addChild(this.graphics)
  }

  update (env, bots, selected, viewMode) {
    this.graphics.clear()
    env.draw(this, viewMode)

    for (const bot of bots) {
      bot.draw(this, viewMode)
    }

    if (selected) {
      this.drawSelected(selected)
      if (this.stickSelected && selected.type === 'bot') {
        const scale = this.getScale()
        const botPosition = selected.getCellPos(this)
        this.setPosition(this.width / 2 - scale * botPosition.x, this.height / 2 - scale * botPosition.y)
      }
    }
  }

  drawSelected (selected) {
    let pos = selected.getCellPos(this)
    const lineSize = 2 / this.getScale()
    if (selected.type === 'bot') {
      this.graphics.lineStyle(lineSize, COLORS.selectedInnerBorder)
      this.graphics.drawEllipse(pos.x, pos.y, this.cellWidth * selected.size / 2 + 2, this.cellHeight * selected.size / 2 + 2)
      this.graphics.lineStyle(lineSize, COLORS.selectedOuterBorder)
      this.graphics.drawEllipse(pos.x, pos.y, this.cellWidth * selected.size / 2 + 2 + lineSize, this.cellHeight * selected.size / 2 + 2 + lineSize)
    } else {
      this.graphics.lineStyle(lineSize, COLORS.selectedInnerBorder)
      this.graphics.drawRect(pos.x - 1, pos.y - 1, this.cellWidth + 2, this.cellHeight + 2)
      this.graphics.lineStyle(lineSize, COLORS.selectedOuterBorder)
      this.graphics.drawRect(pos.x - 1 - lineSize, pos.y - 1 - lineSize, this.cellWidth + 2 + lineSize * 2, this.cellHeight + 2 + lineSize * 2)
    }
  }

  render () {
    this.app.renderer.render(this.app.stage)
  }

  changeScale (delta, x, y) {
    const stage = this.app.stage
    delta = delta < 0 ? 2 : 0.5

    const worldPos = this.toWorldPosition(x, y)
    const newScale = {
      x: stage.scale.x * delta,
      y: stage.scale.y * delta
    }

    const newScreenPos = {
      x: worldPos.x * newScale.x + stage.x,
      y: worldPos.y * newScale.y + stage.y
    }

    stage.x -= (newScreenPos.x - x)
    stage.y -= (newScreenPos.y - y)
    stage.scale.x = newScale.x
    stage.scale.y = newScale.y
  }

  getScale () {
    return this.app.stage.scale.x
  }

  resetScale () {
    this.app.stage.scale.x = 1
    this.app.stage.scale.y = 1
  }

  changePosition (deltaX, deltaY) {
    this.app.stage.x += deltaX
    this.app.stage.y += deltaY
  }

  getPosition () {
    return {
      x: this.app.stage.x,
      y: this.app.stage.y
    }
  }

  setPosition (x, y) {
    this.app.stage.x = x
    this.app.stage.y = y
  }

  resetPosition () {
    this.app.stage.x = 0
    this.app.stage.y = 0
  }

  toWorldPosition (x, y) {
    return {
      x: (x - this.app.stage.x) / this.app.stage.scale.x,
      y: (y - this.app.stage.y) / this.app.stage.scale.y
    }
  }
}
