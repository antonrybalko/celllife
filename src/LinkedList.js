class Node {
  constructor (value) {
    this.data = value
    this.previous = null
    this.next = null
  }
}

export default class LinkedList {
  constructor () {
    this.count = 0
    this.head = null
    this.tail = null
  }

  push (value) {
    const node = new Node(value)

    if (this.head == null) {
      this.head = node
      this.tail = node
    } else {
      this.tail.next = node
      node.previous = this.tail
      this.tail = node
    }

    this.count += 1
    return node
  }

  insert (value) {
    const node = new Node(value)

    if (this.head == null) {
      this.head = node
      this.tail = node
    } else {
      this.head.previous = node
      node.next = this.head
      this.head = node
    }

    this.count++
    return node
  }

  remove (node) {
    if (!(node instanceof Node)) {
      console.log(node)
      throw new Error('node is not instance of Node')
    }
    if (node == null) {
      throw new Error('node is null')
    }

    if (node.previous != null) {
      node.previous.next = node.next
    } else {
      this.head = node.next
    }

    if (node.next != null) {
      node.next.previous = node.previous
    } else {
      this.tail = node.previous
    }

    // Is it necessary to set nulls?
    // Broke loop iteration when node removed
    // node.next = null
    // node.previous = null
    this.count -= 1
  }

  length () {
    return this.count
  }

  isEmpty () {
    return this.head === null
  }

  * values () {
    let current = this.head
    while (current != null) {
      yield current.data
      current = current.next
    }
  }

  [Symbol.iterator] () {
    return this.values()
  }
}
